<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
    	return view('data.index');
    }

    public function login(Request $request){
    	$name = $request->fname;

    	//dd($name);
    	return view('data.index', compact('name'));
    }
}
