<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
    	return view('create');
    }

    public function store(request $request){
    	//dd($request->all());
    	$request ->validate([
    		'judul' => 'required|unique:pertanyaan',
    		'isi' => 'required|unique:pertanyaan',
    		'tanggal_dibuat' => 'required|unique:pertanyaan',
    		'tanggal_diperbaharui' => 'required|unique:pertanyaan',
    		'jawaban_tepat_id' => 'required|unique:pertanyaan',
    		'profiles_id' => 'required|unique:pertanyaan'
    	]);

    	$query = DB::table('pertanyaan')->insert([
    			"judul" => $request["judul"],
    			"isi" => $request["isi"],
    			"tanggal_dibuat" => $request["tanggal_dibuat"],
    			"tanggal_diperbaharui" => $request["tanggal_diperbaharui"],
    			"jawaban_tepat_id" => $request["jawaban_tepat_id"],
    			"profiles_id" => $request["profiles_id"]
    		]);
    	return redirect('/pertanyaan
    		')->with('success', 'Pertanyaan Berhasil Disimpan');

    	public function index(){
    		$pertanyaan = DB::table('pertanyaan') ->get();
    		dd($pertanyaan);
    		return view('index', compact('pertanyaan'));
    	}

    	public function show($id){
    		$pertanyaan = DB::table('pertanyaan') ->where('id', $id)->first();
    		//dd($pertanyaan);
    		return view('show', compact('pertanyaan'));
    	}

    	public function update($id, Request $request){
    		$request->validate([
    			'judul' => 'required|unique:pertanyaan',
    			'isi' => 'required'
    		]);

    		$query = DB::table('pertanyaan')
    				->where('id', $id)
    				->update([
    					'judul' => $request['judul'],
    					'isi' => $request['isi']
    				]);

    		return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    	}

    	public function destroy($id){
    		$query = DB::Table('pertanyaan')->where('id', $id)->delete();
    		return redirect('/pertanyaan')->with('success', 'pertanyaan berhssil didelete');
    	}

    	public function edit($id){
    		$pertanyaan = DB::table('pertanyaan') ->where('id', $id)->first();

    		return view('edit', compact('pertanyaan'));
    	}
    }
}
