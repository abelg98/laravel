@extends('master')

@section('content')
<div class="card card-primary">
	<div class="ml-3 mt-3">
		 <div class="card-header">
		                <h3 class="card-title">Create New Pertanyaan</h3>
		              </div>
		              <!-- /.card-header -->
		              <!-- form start -->
		              <form role="form" action="/pertanyaan" method="POST">
		              	@csrf
		                <div class="card-body">
		                  <div class="form-group">
		                    <label for="judul">Judul</label>
		                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Enter Judul">
		                    @error('judul')
		                    	<div class="alert alert-danger">{{$message}}</div>
		                    @enderror
		                  </div>
		                  <div class="form-group">
		                    <label for="isi">Isi</label>
		                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('judul', '')}} placeholder="Isi">
		                   @error('isi')
		                    	<div class="alert alert-danger">{{$message}}</div>
		                    @enderror
		                  </div>
		                  <div class="form-group">
		                    <label for="tanggal_dibuat">Tanggal Dibuat</label>
		                    <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" value="{{old('judul', '')}}"placeholder="Tanggal Dibuat">
		                  </div>
		                  <div class="form-group">
		                    <label for="tanggal_diperbaharui">Tanggal Diperbaharui</label>
		                    <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" placeholder="Tanggal Diperbaharui">
		                  </div>
		                  <div class="form-group">
		                    <label for="jawaban_tepat_id">Jawaban Id</label>
		                    <input type="text" class="form-control" id="jawaban_tepat_id" name="jawaban_tepat_id" placeholder="Jawaban Id">
		                  </div>
		                  <div class="form-group">
		                    <label for="profiles_id">Profiles Id</label>
		                    <input type="text" class="form-control" id="profiles_id" name="profiles_id" placeholder="Profiles Id">
		                  </div>
             
                                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>


@endsection