@extends('master2')

@section('content')
	<style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
			<form action="/welcome" method="POST">
        @csrf
  				<label for="fname">First name:</label><br><br>
 				<input type="text" id="fname" name="fname" value=""><br><br>
  				<label for="lname">Last name:</label><br><br>
  				<input type="text" id="lname" name="lname" value=""><br><br>
  				<label for="gender">Gender:</label><br><br>
  				<input type="radio" id="male" name="gender" value="male">
  				<label for="male">Male</label><br>
  				<input type="radio" id="female" name="gender" value="female">
  				<label for="female">Female</label><br>
  				<input type="radio" id="other" name="gender" value="other">
  				<label for="other">Other</label><br><br>
  				<label for="nationality">Nationality:</label><br><br>
  				<select id="Nationality" name="Nationality"><br>
    			<option value="indonesian">Indonesian</option>
    			<option value="chinese">Chinese</option>
    			<option value="japanese">Japanese</option>
    			<option value="korean">Korean</option><br>
  				</select><br><br>
  				<label for="languange">Languange Spoken:</label><br><br>
  				  <input type="checkbox" id="languange1" name="languange1" value="Bahasa Indonesia">
  				  <label for="languange1"> Bahasa Indonesia</label><br>
  				  <input type="checkbox" id="languange2" name="languange2" value="English">
  				  <label for="languange2"> English</label><br>
  				  <input type="checkbox" id="languange3" name="languange3" value="Other">
  				  <label for="languange3"> Other</label><br><br>
  				  <label for="bio">Bio:</label><br><br>
  				  <textarea name="message" rows="10" cols="30">
					
				  </textarea><br><br>
  				<input type="submit" value="Sign Up">
			</form> 
@endsection