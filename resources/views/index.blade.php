@extends('master')

@section('content')
	<div class="mt-3 ml-3">
		<div class="card">
              <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              	@if(session('success'))
              		<div class="alert alert-success">
              			{{session('success')}}
              		</div>
              	@endif
              	<a class="btn btn-primary" href="/pertanyaan/create">Create New Pertanyaan</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($pertanyaan as $key => $pertanyaan)
                    <tr>
                    	<td>{{ $key + 1 }}</td>
                    	<td>{{ $pertanyaan -> judul }}</td>
                    	<td>{{ $pertanyaan -> isi }}</td>
                    	<td style="display:flex;">
                    		<a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">Show</a>
                    		<a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info btn-sm">Edit</a>
                    		<form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                    			@csrf
                    			@method('DELETE')
                    			<input type="submit"
 								value="delete" class="btn btn-danger btn-sm">
 							</form>
                     	</td>
                    </tr>
                    @empty
                    	<td colspan="4" align="center">No Pertanyaan</td>
                    @endforelse
                </tbody>
                </table>
              </div>
              <!-- /.card-body -->
             
            </div>
	</div>

@endsection